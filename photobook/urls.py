from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from photobook.galleries import urls as galleries_urls

urlpatterns = [
    path('admin/django-rq/', include('django_rq.urls')),
    path('admin/', admin.site.urls),

    path('api/v1/', include(galleries_urls)),
    path('api/v1/tus/', include(('rest_framework_tus.urls', 'rest_framework_tus'))),
]

# server static and media files
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    try:
        import debug_toolbar
    except ImportError:
        pass
    else:
        urlpatterns += [
            path('__debug__', include(debug_toolbar.urls)),
        ]
