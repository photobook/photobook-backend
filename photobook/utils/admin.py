from typing import Any, Dict, Optional

from django.conf import settings
from django.core.files import File
from django.db import models
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.engine import NoSourceGenerator
from easy_thumbnails.exceptions import InvalidImageFormatError
from easy_thumbnails.files import get_thumbnailer


# Thanks to http://haineault.com/blog/174/
class ThumbnailAdminMixin(object):
    thumbnail_alias: Optional[str] = None
    thumbnail_options: Dict[str, Any] = {'size': (100, 80)}
    thumbnail_image_field_name: str = 'image'
    thumbnail_alt_field_name: Optional[str] = None

    def _thumb(
        self,
        image: File,
        options: Dict[str, Any] = {'size': (90, 90)},
        alt: Optional[str] = None,
    ) -> str:
        """
        Generate a thumbnail and return an image html tag.
        """
        if not image:
            return 'Error...'

        media_url = getattr(settings, 'THUMBNAIL_MEDIA_URL', settings.MEDIA_URL)
        thumbnailer = get_thumbnailer(image)
        if self.thumbnail_alias:
            thumbnail_url = thumbnailer[self.thumbnail_alias]
        else:
            thumbnail_url = thumbnailer.get_thumbnail(options)
        attrs = {
            'src': f'{media_url}{thumbnail_url}',
        }

        if alt is not None:
            attrs['alt'] = alt

        attr_str = ' '.join(f'{attr}="{value}"' for attr, value in attrs.items())
        return mark_safe(f'<img {attr_str} />')

    def thumbnail(self, obj: models.Model) -> str:
        """
        Return a html representation of the requested image.
        """
        kwargs: Dict[str, Any] = {'options': self.thumbnail_options}

        if self.thumbnail_alt_field_name is not None:
            kwargs['alt'] = getattr(obj, self.thumbnail_alt_field_name)

        try:
            return self._thumb(
                getattr(obj, self.thumbnail_image_field_name),
                **kwargs,
            )
        except InvalidImageFormatError:
            return _("<not found>")
        except NoSourceGenerator:
            return _("<invalid image>")
    thumbnail.allow_tags = True  # type: ignore
    thumbnail.short_description = _("Thumbnail")  # type: ignore
