from PIL import Image
from video_encoding.backends import get_backend


def ffmpeg_frame(source, **options):
    backend = get_backend()
    duration = backend.get_media_info(source.path)['duration']
    image_path = backend.get_thumbnail(source.path, at_time=duration / 2)
    return Image.open(image_path)
