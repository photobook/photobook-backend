import os

import pytest
from easy_thumbnails.alias import aliases

from photobook.galleries.models import Photo
from photobook.galleries.tests.factories import PhotoFactory

from ..drf_fields import ThumbnailAliasField


def test_thumbnail_alias_field__invalid_source():
    photo = Photo()
    field = ThumbnailAliasField()
    data = field.to_representation(photo.file)
    assert data == {}


@pytest.mark.django_db
@pytest.mark.parametrize(
    'excluded_fields, expected_fields',
    [
        [[], {'small', 'medium'}],
        [('small',), {'medium'}],
    ]
)
def test_thumbnail_alias_field(settings, excluded_fields, expected_fields):
    settings.THUMBNAIL_ALIASES = {
        '': {
            'small': {'size': (100, 100)},
            'medium': {'size': (0, 300)},
        }
    }
    aliases.populate_from_settings()
    photo = PhotoFactory.create()

    field = ThumbnailAliasField(exclude=excluded_fields)
    data = field.to_representation(photo.file)
    assert data.keys() == expected_fields


@pytest.mark.django_db
def test_thumbnail_alias_field__missing_file():
    photo = PhotoFactory.create()
    os.unlink(photo.file.path)

    field = ThumbnailAliasField()
    data = field.to_representation(photo.file)
    assert data == {}
