import logging
from typing import Dict

from django.conf import settings
from django.core.files import File
from easy_thumbnails.exceptions import InvalidImageFormatError
from easy_thumbnails.files import get_thumbnailer
from rest_framework import serializers

logger = logging.getLogger(__name__)


class ThumbnailAliasField(serializers.Field):
    def __init__(self, *args, **kwargs) -> None:
        self.exclude = kwargs.pop('exclude', [])
        super().__init__(*args, **kwargs)

    def to_representation(self, source: File) -> Dict[str, str]:
        urls = {}
        aliases = set(settings.THUMBNAIL_ALIASES[''].keys())
        aliases = aliases.difference(self.exclude)

        thumbnailer = get_thumbnailer(source)

        for alias in aliases:
            try:
                thumb_url = thumbnailer[alias].url
            except InvalidImageFormatError:
                logger.exception('Could not create Thumbnails')
                return {}
            urls[alias] = get_absolute_url(thumb_url)
        return urls


def get_absolute_url(path):
    domain = settings.SITE_DOMAIN
    return f'http://{domain}{path}'
