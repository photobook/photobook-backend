import enum
import mimetypes

from django.db.models import Model
from easy_thumbnails.signals import saved_file

# https://en.wikipedia.org/wiki/Raw_image_format
RAW_EXTENSIONS = [
    '3fr',  # Hasselblad
    'ari',  # Arri_Alexa
    'arw', 'srf', 'sr2',  # Sony
    'bay',  # Casio
    'cri',  # Cintel
    'crw', 'cr2', 'cr3',  # Canon
    'cap', 'iiq', 'eip',  # Phase_One
    'dcs', 'dcr', 'drf', 'k25', 'kdc',  # Kodak
    'dng',  # Adobe
    'erf',  # Epson
    'fff',  # Imacon/Hasselblad raw
    'mef',  # Mamiya
    'mdc',  # Minolta, Agfa
    'mos',  # Leaf
    'mrw',  # Minolta, Konica Minolta
    'nef', 'nrw',  # Nikon
    'orf',  # Olympus
    'pef', 'ptx',  # Pentax
    'pxn',  # Logitech
    'r3d',  # RED Digital Cinema
    'raf',  # Fuji
    'raw', 'rw2',  # Panasonic
    'raw', 'rwl', 'dng',  # Leica
    'rwz',  # Rawzor
    'srw',  # Samsung
    'x3f',  # Sigma
]

# Required for extensions, which cannot be identified as image or video using
# the `mimetype` module
ADDITIONAL_VIDEO_EXTENSIONS = ['mts', '3gpp']


class FileType(enum.Enum):
    VIDEO = 'video'
    IMAGE = 'image'
    RAW = 'raw'
    OTHER = 'other'

    @staticmethod
    def guess_from_filename(filename: str) -> 'FileType':
        is_raw = any(filename.lower().endswith(f'.{extension}')
                     for extension in RAW_EXTENSIONS)
        if is_raw:
            return FileType.RAW

        mime, _ = mimetypes.guess_type(filename, strict=False)
        # guess_type may returns `None`, use empty string instead
        mime = mime or ''

        if mime.startswith('image'):
            return FileType.IMAGE

        is_video = mime.startswith('video')
        is_video |= any(filename.lower().endswith(f'.{extension}')
                        for extension in ADDITIONAL_VIDEO_EXTENSIONS)
        if is_video:
            return FileType.VIDEO

        return FileType.OTHER


def trigger_thumbnail_generation(instance: Model, fieldname: str) -> None:
    sender = instance.__class__
    fieldfile = getattr(instance, fieldname)
    saved_file.send_robust(sender=sender, fieldfile=fieldfile)
