from django.db.models.signals import post_save
from django.dispatch import receiver
from django_rq import enqueue
from easy_thumbnails.signals import saved_file

from .models import Video


@receiver(post_save, sender=Video)
def convert_video(sender, instance, **kwargs):
    from video_encoding import tasks
    enqueue(
        tasks.convert_all_videos,
        instance._meta.app_label,
        instance._meta.model_name,
        instance.pk,
    )


# NOTE `save_file` is not triggered, when using `FileField.save()` directly
#      In that case you have to trigger the signal by yourself.
#      See https://github.com/SmileyChris/easy-thumbnails/issues/417
@receiver(saved_file)
def generate_thumbnails(sender, fieldfile, **kwargs):
    from . import tasks
    instance = fieldfile.instance
    enqueue(
        tasks.generate_thumbnails,
        instance._meta.app_label,
        instance._meta.model_name,
        instance.pk,
        fieldfile.field.name,
    )
