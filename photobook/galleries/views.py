from django.db.models.query import QuerySet
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
from rest_framework.decorators import action, detail_route
from rest_framework.exceptions import ValidationError
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework_tus import states
from rest_framework_tus.models import Upload

from . import exceptions, models, serializers


class GalleryPagination(PageNumberPagination):
    page_size = 5


class GalleryViewSet(viewsets.ModelViewSet):
    ordering_fields = ('first_day',)
    ordering = ('-first_day',)

    queryset = models.Gallery.objects.all()
    serializer_class = serializers.GallerySerializer
    lookup_url_kwarg = 'uuid'
    lookup_field = 'uuid'

    pagination_class = GalleryPagination

    @detail_route()
    def random(self, request, **options) -> Response:
        """
        Return some random images as preview.
        """
        gallery = self.get_object()

        items = models.GalleryItem.objects.all()
        items = items.filter(gallery=gallery)
        items = items.select_related(
            'gallery',
        )
        items = items.select_subclasses()
        items = items.order_by('?')[:10]

        # items = sorted(items, key=lambda g: g.date_taken)
        serializer = serializers.GalleryItemSerializer(items, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['post'])
    def add_uploaded_item(self, request, **options) -> Response:
        serializer = serializers.UploadedItemSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        upload = get_object_or_404(
            Upload,
            guid=serializer.data['file_id'],
            state=states.DONE,
        )
        gallery = self.get_object()

        try:
            item = gallery.add_uploaded_item(upload)
        except exceptions.RawImage:
            raise ValidationError(
                detail="Raw image files are currently not supported.",
                code=status.HTTP_400_BAD_REQUEST,
            )
        except exceptions.UnsupportedFileType:
            raise ValidationError(
                detail=f"Filetype of `{upload.filename}` is not supported.",
                code=status.HTTP_400_BAD_REQUEST,
            )

        serializer = serializers.GalleryItemSerializer(instance=item)
        return Response(
            serializer.data,  # type: ignore
            status=status.HTTP_201_CREATED,
        )


class GalleryItemViewSet(viewsets.ReadOnlyModelViewSet):
    ordering_fields = ('date_taken',)
    ordering = ('-date_taken',)

    model = models.GalleryItem
    serializer_class = serializers.GalleryItemSerializer
    paginate_by = 30
    paginate_by_param = 'page'

    def get_queryset(self) -> QuerySet:
        queryset = super().get_queryset()
        return queryset.select_subclasses()


class NestedGallerieItemsViewSet(GalleryItemViewSet):
    ordering_fields = ('date_taken',)
    ordering = ('date_taken',)

    def get_queryset(self):
        uuid = self.kwargs.get('uuid')
        try:
            gallery = models.Gallery.objects.get(uuid=uuid)
        except models.Gallery.DoesNotExist:
            raise Http404()

        queryset = models.GalleryItem.objects.filter(gallery=gallery)
        queryset = queryset.select_related(
            'gallery',
        )
        queryset = queryset.select_subclasses()
        return queryset
