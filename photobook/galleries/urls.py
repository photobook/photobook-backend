from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'galleries'

router = DefaultRouter()
router.register('galleries', views.GalleryViewSet, 'gallery')
router.register('items', views.GalleryItemViewSet, 'item')

urlpatterns = [
    path('', include(router.urls)),
    path('galleries/<uuid:uuid>/items/',
         views.NestedGallerieItemsViewSet.as_view({'get': 'list'})),
]
