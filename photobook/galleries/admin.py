from django.contrib import admin
from django.db.models.query import QuerySet
from video_encoding.admin import FormatInline

from photobook.utils.admin import ThumbnailAdminMixin

from . import models


class GalleryItemInline(ThumbnailAdminMixin,
                        admin.TabularInline):
    model = models.GalleryItem
    thumbnail_alias = 'admin'
    thumbnail_image_field_name = 'file'

    fields = ('thumbnail', 'type', 'width', 'height')
    readonly_fields = fields
    extra = 0

    def get_queryset(self, *args, **kwargs) -> QuerySet:
        queryset = super().get_queryset(*args, **kwargs)
        return queryset.select_subclasses()


@admin.register(models.Gallery)
class GalleryAdmin(admin.ModelAdmin):
    inlines = (GalleryItemInline,)

    list_display = ('title', 'owner', 'first_day', 'image_count',
                    'video_count')
    list_filter = ('owner',)


@admin.register(models.Photo)
class PhotoAdmin(ThumbnailAdminMixin,
                 admin.ModelAdmin):
    model = models.Photo
    thumbnail_alias = 'admin'
    thumbnail_image_field_name = 'file'

    list_display = ('thumbnail', 'gallery',
                    'width', 'height')
    list_filter = ('gallery',)


@admin.register(models.Video)
class VideoAdmin(ThumbnailAdminMixin,
                 admin.ModelAdmin):
    inlines = (FormatInline,)

    model = models.Video
    thumbnail_alias = 'admin'
    thumbnail_image_field_name = 'file'

    list_display = ('thumbnail', 'gallery',
                    'width', 'height')
    list_filter = ('gallery',)
