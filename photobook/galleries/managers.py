from django.db.models import Manager
from django.db.models.query import QuerySet
from model_utils.managers import InheritanceQuerySetMixin


class GalleryItemQuerySet(InheritanceQuerySetMixin,
                          QuerySet):

    def photos(self) -> QuerySet:
        return self.filter(type='photo').select_subclasses('photo')

    def videos(self) -> QuerySet:
        return self.filter(type='video').select_subclasses('video')


GalleryItemManager = Manager.from_queryset(GalleryItemQuerySet)
