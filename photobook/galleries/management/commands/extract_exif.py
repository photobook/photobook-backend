import logging

from django.apps import apps
from django.core.management.base import BaseCommand
from exiffield.fields import ExifField
from tqdm import tqdm

logger = logging.getLogger(__name__)


def is_photobook_model(model) -> bool:
    return 'photobook' in model.__module__


def is_exif_field(field) -> bool:
    return isinstance(field, ExifField)


class Command(BaseCommand):
    def handle(self, **options) -> None:
        models = apps.get_models()
        models = filter(is_photobook_model, models)

        for model in models:
            all_fields = model._meta.get_fields()
            fields = list(filter(is_exif_field, all_fields))
            if fields:
                fieldnames = [field.name for field in fields]
                self.extract_exif(model, fieldnames)

    def extract_exif(self, model, fieldnames) -> None:
        queryset = model._default_manager.all()
        for instance in tqdm(queryset, total=queryset.count(), desc=model.__name__):
            for fieldname in fieldnames:
                field = instance._meta.get_field(fieldname)
                field.update_exif(instance, commit=True, force=True)
