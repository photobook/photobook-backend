import argparse
from pathlib import Path
from typing import Generator, List

from django.contrib.auth import get_user_model
from django.core.files import File
from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_date

from photobook.galleries import exceptions, models, utils


def filter_files(
    gen: Generator[Path, None, None]
) -> Generator[Path, None, None]:
    for path in gen:
        if path.is_file():
            yield path


class Command(BaseCommand):
    def add_arguments(self, parser: argparse.ArgumentParser):
        parser.add_argument('username', type=str)
        parser.add_argument('gallery_path', type=str)
        parser.add_argument('date', type=str)
        parser.add_argument('gallery_name', type=str)

    def handle(self, username: str, gallery_path: str, date: str, gallery_name: str,
               **options) -> None:
        User = get_user_model()
        owner, _ = User.objects.get_or_create(username=username)

        # create gallery
        first_day = parse_date(date)
        gallery, created = models.Gallery.objects.get_or_create(
            first_day=first_day,
            owner=owner,
            title=gallery_name,
        )
        if created:
            print(f'> Importing gallery "{gallery_name}" ({gallery.pk})')
        else:
            print(f'> Updating gallery "{gallery_name}" ({gallery.pk})')

        # keep track of `raw` photos
        raw_paths: List[Path] = []

        # find all relevant files
        path = Path(gallery_path)
        for path in filter_files(path.rglob('*.*')):
            filetype = utils.FileType.guess_from_filename(path.name)
            try:
                model_class = models.GalleryItem.get_model_for_filetype(filetype)
            except exceptions.RawImage:
                raw_paths.append(path)
                continue
            except exceptions.UnsupportedFileType:
                print(f'Ignored `{path}`')
                continue

            # add file to gallery
            item, created = model_class.objects.get_or_create(
                gallery=gallery,
                original_filename=path.name,
            )
            if created:
                print(f'>> Imported `{path}`')
                item.file.save(path.name, File(open(path, 'rb')))
                utils.trigger_thumbnail_generation(item, 'file')
            else:
                print(f'>> Already in gallery `{path}`')
                if not item.file or not Path(item.file.path).is_file():
                    print(f'>>> Add missing file `{path}`')
                    item.file.save(path.name, File(open(path, 'rb')))
                    utils.trigger_thumbnail_generation(item, 'file')

        # handle raw photos
        for path in raw_paths:
            try:
                photo = models.Photo.objects.get(
                    gallery=gallery,
                    original_filename__startswith=f'{path.stem}.',
                )
            except models.Photo.DoesNotExist:
                print(f'Could not find corresponding photo for `{path}`')
                continue

            # NOTE: No need to generate thumbnails
            photo.raw.save(path.name, File(open(path, 'rb')))
            print(f'Attached raw `{path}` to {photo}.')
