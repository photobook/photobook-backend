import logging
from pathlib import Path

from django.apps import apps
from django.core.management.base import BaseCommand
from django.db import models
from easy_thumbnails.engine import NoSourceGenerator
from easy_thumbnails.files import generate_all_aliases
from tqdm import tqdm
from video_encoding.fields import VideoField

logger = logging.getLogger(__name__)


def is_photobook_model(model) -> bool:
    return 'photobook' in model.__module__


def is_media_field(field) -> bool:
    return isinstance(field, models.ImageField) or isinstance(field, VideoField)


class Command(BaseCommand):
    def handle(self, **options) -> None:
        models = apps.get_models()
        models = filter(is_photobook_model, models)

        for model in models:
            all_fields = model._meta.get_fields()
            fields = list(filter(is_media_field, all_fields))
            if fields:
                fieldnames = [field.name for field in fields]
                self.generate_thumbnails(model, fieldnames)

    def generate_thumbnails(self, model, fieldnames) -> None:
        queryset = model._default_manager.all()
        for instance in tqdm(queryset, total=queryset.count(), desc=model.__name__):
            for fieldname in fieldnames:
                field = getattr(instance, fieldname)
                if not Path(field.path).is_file():
                    logger.debug(
                        f'File missing {field.path} from gallery '
                        f'{instance.gallery.uuid}'
                    )
                    continue
                try:
                    generate_all_aliases(field, include_global=True)
                except NoSourceGenerator:
                    logger.exception(
                        f'Could not generate thumbnail from {field.path} '
                        f'({instance.gallery.uuid})'
                    )
