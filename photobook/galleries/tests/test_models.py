import pytest

from .. import exceptions, models, utils


@pytest.mark.parametrize(
    'filetype, model',
    [
        (utils.FileType.IMAGE, models.Photo),
        (utils.FileType.VIDEO, models.Video),
    ]
)
def test_galleryitem__get_model_for_filetype(filetype, model):
    assert models.GalleryItem.get_model_for_filetype(filetype) == model


@pytest.mark.parametrize(
    'filetype, exception',
    [
        (utils.FileType.RAW, exceptions.RawImage),
        (utils.FileType.OTHER, exceptions.UnsupportedFileType),
    ]
)
def test_galleryitem__get_model_for_filetype__unsupported_type(filetype, exception):
    with pytest.raises(exception):
        models.GalleryItem.get_model_for_filetype(filetype)
