import pytest
from video_encoding.models import Format

from .. import serializers


@pytest.mark.parametrize(
    'format_key, size, extension, mime_type, codecs',
    (
        ('webm_sd', (480, 360), 'webm', 'video/webm', 'vp9,vorbis'),
        ('webm_hd', (720, 580), 'webm', 'video/webm', 'vp9,vorbis'),
        # TODO codec is hardcoded
        ('mp4_sd', (480, 360), 'mp4', 'video/mp4', 'vp9,vorbis'),
        ('mp4_hd', (720, 580), 'mp4', 'video/mp4', 'vp9,vorbis'),
    ),
)
def test_formatserializer(
    mocker, format_key, size, extension, mime_type, codecs,
):
    width, height = size
    format_ = Format(
        format=format_key,
        height=height,
        width=width,
    )
    mocked_file = mocker.MagicMock()
    mocked_file.width = width
    mocked_file.height = height
    mocked_file.url = f'/media/foo.{extension}'
    format_.file = mocked_file

    format_data = serializers.FormatSerializer(format_).data
    assert format_data.keys() == {'url', 'mime_type', 'width', 'height', 'codecs'}
    assert format_data['mime_type'] == mime_type
    assert format_data['codecs'] == codecs
    assert format_data['url'].startswith('http')
    assert format_data['height'] == height
    assert format_data['width'] == width


def test_uploaded_item_serializer():
    payload = {'file_id': 'fooo.jpg'}
    serializer = serializers.UploadedItemSerializer(data=payload)
    assert serializer.is_valid()
    assert serializer.data == payload
