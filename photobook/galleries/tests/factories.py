import datetime
import tempfile

import factory
import factory.fuzzy
from django.contrib.auth import get_user_model
from PIL import Image
from rest_framework_tus import states
from rest_framework_tus.models import get_upload_model

from .. import models


class UserFactory(factory.django.DjangoModelFactory):
    last_name = factory.Faker('last_name')
    first_name = factory.Faker('first_name')
    username = factory.Faker('name')
    username = factory.LazyAttribute(
        lambda o: f'{o.first_name}.{o.last_name}'.lower(),
    )
    email = factory.LazyAttribute(lambda o: f'{o.username}@example.com')

    class Meta:
        model = get_user_model()


class GalleryFactory(factory.django.DjangoModelFactory):
    owner = factory.SubFactory(UserFactory)
    uuid = factory.Faker('uuid4')
    title = factory.Faker('sentence')
    description = factory.Faker('paragraph')
    first_day = factory.fuzzy.FuzzyDate(datetime.date.today())

    class Meta:
        model = models.Gallery


class GalleryItemFactory(factory.DjangoModelFactory):
    gallery = factory.SubFactory(GalleryFactory)


class PhotoFactory(GalleryItemFactory):
    file = factory.django.ImageField()

    class Meta:
        model = models.Photo


class UploadFactory(factory.DjangoModelFactory):
    guid = factory.Faker('uuid4')
    state = states.DONE

    filename = factory.Faker('file_name', extension='jpg')

    class Meta:
        model = get_upload_model()

    @factory.lazy_attribute
    def temporary_file_path(self):
        thumb = Image.new('RGB', (100, 100), 'blue')
        with tempfile.NamedTemporaryFile(suffix='.jpg', delete=False) as fp:
            thumb.save(fp, format='JPEG')
        return fp.name
