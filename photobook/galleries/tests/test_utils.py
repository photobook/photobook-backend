import pytest

from .. import utils


@pytest.mark.parametrize(
    'extension, filetype',
    [
        (extension, utils.FileType.IMAGE)
        for extension in ['jpg', 'jpeg', 'tiff']
    ] + [
        (extension, utils.FileType.VIDEO)
        for extension in ['mov', '3gp', '3gpp', 'mts', 'mp4', 'avi']
    ] + [
        (extension, utils.FileType.RAW)
        for extension in utils.RAW_EXTENSIONS
    ] + [
        (extension, utils.FileType.OTHER)
        for extension in ['doc', 'pdf', 'xlsx', '']
    ]
)
def test_filetype__guess_from_filename(extension, filetype):
    filename = f'foo.{extension}'
    assert utils.FileType.guess_from_filename(filename) == filetype
