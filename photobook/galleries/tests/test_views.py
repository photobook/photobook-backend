import pytest
from rest_framework import status
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework_tus import states
from rest_framework_tus.models import Upload

from . import factories
from .. import models, serializers


@pytest.fixture
def request_factory():
    return APIRequestFactory()


@pytest.fixture
def client():
    return APIClient()


@pytest.mark.django_db
def test_gallery__add_uploaded_item__missing_upload(faker, client):
    gallery = factories.GalleryFactory.create()

    response = client.post(
        f'/api/v1/galleries/{gallery.uuid}/add_uploaded_item/',
        {'file_id': faker.uuid4()},
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.parametrize(
    'upload_state',
    [
        states.INITIAL,
        states.RECEIVING,
        states.SAVING,
    ]
)
def test_gallery__add_uploaded_item__upload_not_ready(
    client, upload_state,
):
    gallery = factories.GalleryFactory.create()
    upload = factories.UploadFactory.create(state=upload_state)

    response = client.post(
        f'/api/v1/galleries/{gallery.uuid}/add_uploaded_item/',
        {'file_id': upload.guid},
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.parametrize(
    'filename, error_message',
    [
        ('foo.dng', 'Raw image files are'),
        ('foo.docx', 'Filetype of'),
    ]
)
def test_gallery__add_uploaded_item__invalid_file(
    client, filename, error_message,
):
    gallery = factories.GalleryFactory.create()
    upload = factories.UploadFactory.create(filename=filename)

    response = client.post(
        f'/api/v1/galleries/{gallery.uuid}/add_uploaded_item/',
        {'file_id': upload.guid},
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert error_message in response.json()[0]


@pytest.mark.django_db
def test_gallery__add_uploaded_item(client):
    gallery = factories.GalleryFactory.create()
    upload = factories.UploadFactory.create()

    response = client.post(
        f'/api/v1/galleries/{gallery.uuid}/add_uploaded_item/',
        {'file_id': upload.guid},
    )
    assert response.status_code == status.HTTP_201_CREATED
    assert models.GalleryItem.objects.count() == 1

    item = models.GalleryItem.objects.select_subclasses().get()
    assert item.original_filename == upload.filename

    # uploaded file should have been removed
    assert not Upload.objects.exists()

    assert response.json() == serializers.GalleryItemSerializer(instance=item).data
