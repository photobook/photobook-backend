class PhotobookException(Exception):
    pass


class RawImage(PhotobookException):
    pass


class UnsupportedFileType(PhotobookException):
    pass
