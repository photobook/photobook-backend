import os
import uuid
from typing import Type, Union

from autoslug import AutoSlugField
from choicesenum.django.fields import EnumCharField
from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.core.files import File
from django.db import models
from django.utils.translation import ugettext_lazy as _
from exiffield import getters
from exiffield.fields import ExifField
from model_utils.models import TimeStampedModel
from rest_framework_tus.models import Upload
from video_encoding.fields import VideoField
from video_encoding.models import Format

from . import exceptions, managers, utils


def get_gallery_path(gallery: 'Gallery') -> str:
    return os.path.join('galleries', gallery.owner.username, str(gallery.uuid))


def get_galleryitem_path(item: 'GalleryItem', path: str) -> str:
    p = get_gallery_path(item.gallery)
    return os.path.join(p, os.path.basename(path))


class Gallery(TimeStampedModel, models.Model):
    owner = models.ForeignKey(
        on_delete=models.CASCADE,
        to=settings.AUTH_USER_MODEL,
    )
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        primary_key=True,
    )

    title = models.CharField(
        max_length=255,
        verbose_name=_("Title"),
    )
    slug = AutoSlugField(
        populate_from='title',
    )
    description = models.TextField(
        blank=True,
        verbose_name=_("description"),
    )

    first_day = models.DateField(
        blank=True,
        null=True,
    )

    def _item_count(self) -> int:
        return self.items.count()
    _item_count.short_description = _("Items")  # type: ignore
    item_count = property(_item_count)

    def _image_count(self) -> int:
        return GalleryItem.objects.photos().filter(gallery=self).count()
    _image_count.short_description = _("Images")  # type: ignore
    image_count = property(_image_count)

    def _video_count(self) -> int:
        return GalleryItem.objects.videos().filter(gallery=self).count()
    _video_count.short_description = _("Videos")  # type: ignore
    video_count = property(_video_count)

    class Meta:
        ordering = ['-first_day']
        verbose_name = _("Gallery")
        verbose_name_plural = _("Galleries")

    def __str__(self):
        return self.title

    def add_uploaded_item(
        self,
        upload: Upload,
        delete: bool = True,
    ) -> Union['Photo', 'Video']:
        """
        Create a `GalleryItem` from an uploaded image.

        The file type (video, image) is guessed from the filename. If the
        extension is unknown an `UnsupportedFile` exception is thrown.

        NOTE: raw images are currently not supported. Hence a `RawImage`
              exception will be thrown instead.
        """

        filetype = utils.FileType.guess_from_filename(
            upload.filename,
        )
        # may raises exceptions
        model_class = GalleryItem.get_model_for_filetype(filetype)

        item = model_class.objects.create(
            gallery=self,
            original_filename=upload.filename,
        )
        item.file.save(upload.filename, File(open(upload.temporary_file_path, 'rb')))
        utils.trigger_thumbnail_generation(item, 'file')

        if delete:
            upload.delete()

        return item


class GalleryItem(TimeStampedModel, models.Model):
    gallery = models.ForeignKey(
        on_delete=models.CASCADE,
        related_name='items',
        to=Gallery,
    )
    type = models.CharField(
        editable=False,
        max_length=50,
    )
    description = models.TextField(
        blank=True,
    )

    original_filename = models.CharField(
        editable=False,
        max_length=100,
    )

    filesize = models.PositiveIntegerField(
        default=0,
        editable=False,
    )
    width = models.PositiveIntegerField(
        default=0,
        editable=False,
    )
    height = models.PositiveIntegerField(
        default=0,
        editable=False,
    )
    orientation = EnumCharField(
        default=getters.Orientation.LANDSCAPE,
        editable=False,
        enum=getters.Orientation,
        max_length=50,
    )

    objects = managers.GalleryItemManager()

    class Meta:  # type: ignore
        verbose_name = _("Gallery items")
        verbose_name_plural = _("Gallery items")

    def __str__(self) -> str:
        return f'{self._meta.verbose_name}: {self.original_filename}'

    @staticmethod
    def get_model_for_filetype(
        filetype: utils.FileType,
    ) -> Union[Type['Photo'], Type['Video']]:
        """
        Return matching subclass of `GalleryItem`.

        Raises
        ------
        RawImage
            There is currently no model for a raw image.
        UnsupportedFileType
            If a model for an unsupported filetype is requested.
        """
        if filetype == utils.FileType.IMAGE:
            return Photo
        if filetype == utils.FileType.VIDEO:
            return Video
        if filetype == utils.FileType.RAW:
            raise exceptions.RawImage()

        raise exceptions.UnsupportedFileType()

    def save(self, *args, **kwargs) -> None:
        self.type = self.__class__.__name__.lower()

        if self.file:
            self.filesize = self.file.size

        super().save(*args, **kwargs)


class Photo(GalleryItem):
    file = models.ImageField(
        height_field='height',
        upload_to=get_galleryitem_path,
        verbose_name=_("Original"),
        width_field='width',
    )
    raw = models.FileField(
        blank=True,
        null=True,
        upload_to=get_galleryitem_path,
        verbose_name=_("Raw"),
    )
    exif = ExifField(
        default='{}',
        denormalized_fields={
            'orientation': getters.get_orientation,
        },
        source='file',
        null=True,
    )

    class Meta:  # type: ignore
        verbose_name = _("Photo")
        verbose_name_plural = _("Photos")


class Video(GalleryItem):
    file = VideoField(
        duration_field='duration',
        height_field='height',
        upload_to=get_galleryitem_path,
        verbose_name=_("Video"),
        width_field='width',
    )
    duration = models.PositiveIntegerField(
        default=0,
        editable=False,
    )
    exif = ExifField(
        default='{}',
        denormalized_fields={
            'orientation': getters.get_orientation,
        },
        source='file',
        null=True,
    )

    format_set = GenericRelation(Format)

    class Meta:  # type: ignore
        verbose_name = _("Video")
        verbose_name_plural = _("Videos")
