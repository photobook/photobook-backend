from django.apps import AppConfig


class GalleriesAppConfig(AppConfig):
    name = 'photobook.galleries'

    def ready(self):
        from . import signals  # NOQA
