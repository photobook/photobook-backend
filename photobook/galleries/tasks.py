from django.apps import apps
from easy_thumbnails.files import generate_all_aliases


def generate_thumbnails(app_label, model_name, object_pk, fieldname):
    Model = apps.get_model(app_label=app_label, model_name=model_name)
    instance = Model.objects.get(pk=object_pk)
    field = getattr(instance, fieldname)

    generate_all_aliases(field, include_global=True)
