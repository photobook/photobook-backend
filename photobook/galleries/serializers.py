import logging

from exiffield.getters import Orientation
from rest_framework import serializers
from video_encoding.models import Format

from photobook.utils.drf_fields import ThumbnailAliasField, get_absolute_url

from . import models

logger = logging.getLogger()


class GallerySerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(
        source='uuid',
    )
    owner = serializers.ReadOnlyField(
        source='owner.username',
    )
    date = serializers.ReadOnlyField(
        source='first_day',
    )

    class Meta:
        model = models.Gallery
        fields = ('id', 'slug', 'title', 'description',
                  'date', 'owner')


class BaseGalleryItemSerializer(serializers.ModelSerializer):
    gallery_id = serializers.PrimaryKeyRelatedField(
        pk_field=serializers.UUIDField(format='hex_verbose'),
        queryset=models.Gallery.objects.all().only('uuid'),
    )
    thumbnails = ThumbnailAliasField(
        exclude=('admin',),
        source='file',
    )
    filename = serializers.ReadOnlyField(
        source='original_filename',
    )
    original = serializers.SerializerMethodField()
    height = serializers.SerializerMethodField()
    width = serializers.SerializerMethodField()

    class Meta:
        model = models.GalleryItem
        fields = ['id', 'gallery_id', 'type',
                  'thumbnails', 'filename', 'original',
                  'filesize', 'width', 'height']

    def get_width(self, obj):
        if obj.orientation == Orientation.PORTRAIT:
            return obj.height
        return obj.width

    def get_height(self, obj):
        if obj.orientation == Orientation.PORTRAIT:
            return obj.width
        return obj.height

    def get_original(self, obj):
        try:
            return get_absolute_url(obj.file.url)
        except ValueError:
            logger.error(
                'Item %s has no file attached.',
                obj.pk,
            )
        return ''


class PhotoSerializer(BaseGalleryItemSerializer):

    class Meta(BaseGalleryItemSerializer.Meta):
        model = models.Photo


class FormatSerializer(serializers.ModelSerializer):
    mime_type = serializers.SerializerMethodField()
    codecs = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()

    class Meta:
        model = Format
        fields = ('height', 'width', 'mime_type', 'codecs', 'url')

    def get_mime_type(self, obj):
        extension = obj.file.url.rsplit('.', 1)[-1]
        return f'video/{extension}'

    def get_codecs(self, obj):
        # TODO hardcoded for now
        return 'vp9,vorbis'

    def get_url(self, obj):
        try:
            return get_absolute_url(obj.file.url)
        except ValueError:
            logger.error(
                'Item %s has no file attached.',
                obj.pk,
            )
        return ''


class VideoSerializer(BaseGalleryItemSerializer):
    formats = serializers.SerializerMethodField()

    class Meta(BaseGalleryItemSerializer.Meta):
        model = models.Video
        fields = BaseGalleryItemSerializer.Meta.fields + ['duration', 'formats']

    def get_formats(self, obj):
        formats = obj.format_set.complete().all()
        return FormatSerializer(formats, many=True).data


class GalleryItemSerializer(BaseGalleryItemSerializer):
    def to_representation(self, obj):
        """
        Uses a different serializer for conversion.
        """
        if isinstance(obj, models.Photo):
            return PhotoSerializer(obj).data
        elif isinstance(obj, models.Video):
            return VideoSerializer(obj).data
        return super().to_representation(obj)


class UploadedItemSerializer(serializers.Serializer):
    file_id = serializers.CharField()
