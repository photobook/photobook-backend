=========
Photobook
=========

Google-Photos like app.


Requirements
============

- Python 3.6
- ffmpeg
- `exiftool <https://www.sno.phy.queensu.ca/~phil/exiftool/>`_


Developement
============

Requirements
------------

- Python 3.6
- Poetry >= 0.12

Setup Backend
-------------

Clone the repository and run the following commands from within project directory.

   python3.6 -m venv .venv
   poetry install
   poetry shell


Configure the application using `env.example` and generate the database schema

   ./manage.py migrate

Run the application using

   ./manage.py runserver


Setup Frontend
--------------

The fronend is a VueJS applications and needs to be installed separately.
